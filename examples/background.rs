extern crate dynpool;

use dynpool::{Decision, Scale, System, Pool};


use std::sync::{atomic::AtomicUsize, atomic::Ordering::Relaxed};
use std::thread::sleep;
use std::time::Duration;

struct Work {
    count: u32,
    index: usize,
}

struct Sys(String, AtomicUsize);

impl System for Sys {
    type Data = Work;

    fn init(&self, index: usize) -> Work {
        println!("OPENED {} on {}.", self.0, index);
        Work {
            count: 0,
            index,
        }
    }

    fn close(&self, Work { count, index }: Work) {
        println!("CLOSED {} on {} after {} prints.", self.0, index, count);
    }

    fn scale(&self) -> Scale {
        match self.1.load(Relaxed) {
            0 => Scale::Shutdown,
            n => Scale::Mixed { active: n, max_inactive: 2 },
        }
    }

    fn work(&self, work: &mut Work) -> Decision {
        work.count += 1;
        println!("{} #{} on {}", self.0, work.count, work.index);
        sleep(Duration::from_millis(500));

        if work.count % 2 == 0 {
            Decision::Again
        } else {
            Decision::Incomplete
        }
    }
}


fn main() {
    let hand = Pool::start_bg(Sys("hello".to_owned(), AtomicUsize::new(1)));
    sleep(Duration::from_millis(1250));
    hand.system().1.store(5, Relaxed);
    sleep(Duration::from_secs(1));
    hand.system().1.store(2, Relaxed);
    sleep(Duration::from_secs(1));

    let hand = hand.swap_system(Sys("world".to_owned(), AtomicUsize::new(3)));
    sleep(Duration::from_secs(2));
    hand.system().1.store(1, Relaxed);
    sleep(Duration::from_secs(2));
    hand.system().1.store(4, Relaxed);
    sleep(Duration::from_secs(1));
    hand.system().1.store(0, Relaxed);

    hand.join().unwrap();
}

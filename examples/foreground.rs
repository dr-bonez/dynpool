extern crate dynpool;
use dynpool::*;
use std::thread::sleep;
use std::time::{Duration, Instant};

struct Printer(Instant);

impl System for Printer {
    type Data = String;

    // How many threads? The pool will scale up over time!
    fn scale(&self) -> Scale {
        let time = self.0.elapsed();
        let ms = time.as_secs() * 1000 + time.subsec_millis() as u64;
        match ms {
            0...200 => Scale::active(1),
            201...400 => Scale::active(2),
            401...600 => Scale::active(3),
            601...800 => Scale::active(4),
            _ => Scale::shutdown(),
        }
    }

    // Pick a string for each thread.
    fn init(&self, index: usize) -> String {
        match index {
            0 => "Hello",
            1 => "Hola",
            2 => "Bonjour",
            3 => "Ciao",
            _ => unreachable!(),
        }.to_owned()
    }

    // Do work on several threads!
    fn work(&self, text: &mut String) -> Decision {
        println!("{}", text);
        *text += " Again";
        sleep(Duration::from_millis(100));
        Decision::Again
    }
}

fn main() {
    Pool::start_fg(Printer(Instant::now())).unwrap();
    println!("This is the end!");
}

use std::sync::atomic::{AtomicUsize, Ordering};

#[cfg(not(feature="nightly"))]
pub fn atomic_max(atom: &AtomicUsize, val: usize, ord: Ordering) -> usize {
    let mut prev = atom.load(ord);
    loop {
        match atom.compare_exchange_weak(prev, prev.max(val), ord, ord) {
            Ok(prev) => return prev,
            Err(next_prev) => prev = next_prev
        }
    }
}

#[cfg(feature="nightly")]
pub fn atomic_max(atom: &AtomicUsize, val: usize, ord: Ordering) -> usize {
    atom.fetch_max(val, ord)
}
